package br.ufc.felipe.sippalite;

import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.image.ImageInfo;

import br.ufc.felipe.backendlogin.SIPPALogger;
import br.ufc.felipe.backendlogin.model.Discipline;
import br.ufc.felipe.backendlogin.model.Proof;
import br.ufc.felipe.backendlogin.model.Student;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.login_sippa_logo)
    SimpleDraweeView viewSippaLogo;

    @BindView(R.id.login_input_registration)
    EditText viewInputRegistration;

    @BindView(R.id.login_input_password)
    EditText viewInputPassword;

    @BindView(R.id.login_captcha)
    SimpleDraweeView viewCaptcha;

    @BindView(R.id.login_input_captcha)
    EditText viewInputCaptcha;

    @BindView(R.id.screen_loading)
    View viewScreenLoading;

    @BindView(R.id.screen_login)
    View viewScreenLogin;

    @BindView(R.id.screen_student)
    View viewScreenStudent;

    @BindView(R.id.student_disciplines)
    LinearLayout layoutStudentDisciplines;

    @BindView(R.id.student_name)
    TextView viewStudentName;

    SIPPALogger logger = new SIPPALogger();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this, config);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SIPPALogger.disableSslVerification();

    }

    @Override
    protected void onStart() {
        super.onStart();

        showLogin();
    }

    public void showLoadingScreen() {
        viewScreenLoading.bringToFront();
    }

    public void showStudent() {
        showLoadingScreen();
        updateStudentInfo();
    }

    public void showStudentScreen() {
        viewScreenStudent.bringToFront();
    }

    public void showLogin() {
        showLoadingScreen();
        updateLogin();
        showLoginScreen();
    }

    public void showLoginScreen() {
        viewScreenLogin.bringToFront();
    }

    public void onLogin(View view) {

        showStudent();

    }

    private void updateLogin() {

        ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet( String id, @Nullable ImageInfo imageInfo, @Nullable Animatable anim) {
                //makeToast("Image received: " + imageInfo.toString());
            }

            @Override
            public void onIntermediateImageSet(String id, @Nullable ImageInfo imageInfo) {

            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                //makeToast("Image load failed: " + throwable.toString());
                //Log.i("DEBUG", throwable.toString());
            }
        };

        Uri uri;
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setControllerListener(controllerListener)
                .setUri(SIPPALogger.URL_SIPPA_LOGO)
                .build();
        //viewSippaLogo.setController(controller);
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.clearCaches();
        viewCaptcha.setImageURI(SIPPALogger.URL_CAPTCHA);

    }

    private void updateStudentInfo() {

        logger.login(
                viewInputRegistration.getText().toString(),
                viewInputPassword.getText().toString(),
                viewInputCaptcha.getText().toString(),
                new SIPPALogger.LoginListener() {
                    @Override
                    public void loginSuccess(final Student student) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                viewStudentName.setText(student.getName());
                                for(Discipline discipline: student.getDisciplines()) {
                                    View disciplineView = View.inflate(MainActivity.this, R.layout.item_discipline, null);
                                    ((TextView)disciplineView.findViewById(R.id.student_discipline_code_name))
                                            .setText(discipline.getCode() + " - " + discipline.getName());
                                    ((TextView)disciplineView.findViewById(R.id.student_discipline_teacher))
                                            .setText(discipline.getTeacher());
                                    ((TextView)disciplineView.findViewById(R.id.student_discipline_period))
                                            .setText(discipline.getPeriod());
                                    ((TextView)disciplineView.findViewById(R.id.student_discipline_frequency))
                                            .setText(discipline.getFrequency() + "%");

                                    layoutStudentDisciplines.addView(disciplineView);

                                    LinearLayout layoutStudentsDisciplinesProofs = (LinearLayout) disciplineView.findViewById(R.id.student_discipline_proofs);
                                    for(Proof proof: discipline.getProofs()) {
                                        View proofView = View.inflate(MainActivity.this, R.layout.item_proof, null);
                                        ((TextView)proofView.findViewById(R.id.student_discipline_proof_name))
                                                .setText(proof.getName());
                                        ((TextView)proofView.findViewById(R.id.student_discipline_proof_value))
                                                .setText(proof.getValue());
                                        layoutStudentsDisciplinesProofs.addView(proofView);
                                    }
                                }

                                makeToast(getString(R.string.toast_login_success));
                                showStudentScreen();
                            }
                        });

                    }

                    @Override
                    public void loginError() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                makeToast(getString(R.string.toast_login_error));
                                showLogin();
                            }
                        });
                    }
                }
        );

    }

    public void makeToast(final String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
