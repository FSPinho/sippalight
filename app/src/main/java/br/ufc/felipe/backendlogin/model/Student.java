package br.ufc.felipe.backendlogin.model;

import java.util.ArrayList;
import java.util.List;

public class Student {

	private String name;
	private String registration;

	private List<Discipline> disciplines = new ArrayList<Discipline>();

	public Student() {
	}

	public Student(String name, String registration) {
		this.name = name;
		this.registration = registration;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public List<Discipline> getDisciplines() {
		return disciplines;
	}

	public void setDisciplines(List<Discipline> disciplines) {
		this.disciplines = disciplines;
	}

	public void addDiscipline(Discipline discipline) {
		this.disciplines.add(discipline);
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", registration=" + registration + ", disciplines=" + disciplines + "]";
	}

}
