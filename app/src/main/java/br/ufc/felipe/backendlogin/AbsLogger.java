package br.ufc.felipe.backendlogin;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public abstract class AbsLogger {

	private static final String USER_AGENT = "Mozilla/5.0";

	private List<String> cookies = new ArrayList<>();
	private HttpsURLConnection conn;

	private Timer timer = new Timer();

	private static boolean debugEnabled = true;

	protected static void log(String log) {
		if (debugEnabled)
			Log.i("ADS_LOGGER", log);
	}

	protected boolean isDebugEnabled() {
		return debugEnabled;
	}

	protected void setDebugEnabled(boolean debugEnabled) {
		AbsLogger.debugEnabled = debugEnabled;
	}

	protected void doInBackground(final Runnable runnable) {

		timer.schedule(new TimerTask() {

			@Override
			public void run() {

				runnable.run();

			}

		}, 0);

	}

	protected Response sendPost(String url, String postParams) throws Exception {

		URL obj = new URL(url);
		conn = (HttpsURLConnection) obj.openConnection();

		conn.setUseCaches(false);
		conn.setRequestMethod("POST");
		// conn.setRequestProperty("Host", "accounts.google.com");
		conn.setRequestProperty("User-Agent", USER_AGENT);
		conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		for (String cookie : this.cookies) {
			conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
		}
		conn.setRequestProperty("Connection", "keep-alive");
		conn.setRequestProperty("Referer", "https://accounts.google.com/ServiceLoginAuth");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", Integer.toString(postParams.length()));

		conn.setDoOutput(true);
		conn.setDoInput(true);

		DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
		wr.writeBytes(postParams);
		wr.flush();
		wr.close();

		int responseCode = conn.getResponseCode();
		log("\nSending 'POST' request to URL : " + url);
		log("Post parameters : " + postParams);
		log("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}

		in.close();

		log("Response Body : " + response.toString());

		return new Response(response.toString(), responseCode);

	}

	protected Response getPageContent(String url) throws Exception {

		URL obj = new URL(url);
		conn = (HttpsURLConnection) obj.openConnection();

		conn.setRequestMethod("GET");
		conn.setUseCaches(false);

		conn.setRequestProperty("User-Agent", USER_AGENT);
		conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		if (cookies != null) {
			for (String cookie : this.cookies) {
				conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
			}
		}

		int responseCode = conn.getResponseCode();
		log("\nSending 'GET' request to URL : " + url);
		log("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}

		in.close();

		cookies = conn.getHeaderFields().get("Set-Cookie");

		log("Response Body : " + response.toString());

		return new Response(response.toString(), responseCode);

	}

	public static void disableSslVerification() {

		log("Disable SSL verification...");

		try {

			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			HostnameVerifier allHostsValid = new HostnameVerifier() {

				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			log("Disable SSL verification error!");
			e.printStackTrace();
		}

		log("Disable SSL verification done!");

	}

	protected List<String> getCookies() {
		return cookies;
	}

	protected void setCookies(List<String> cookies) {
		this.cookies = cookies;
	}

	protected HttpsURLConnection getConn() {
		return conn;
	}

	protected void setConn(HttpsURLConnection conn) {
		this.conn = conn;
	}

	protected class Response {
		String text;
		int code;

		public Response(String text, int code) {
			this.text = text;
			this.code = code;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

	}

}
