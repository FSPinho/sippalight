package br.ufc.felipe.backendlogin.model;

import java.util.ArrayList;
import java.util.List;

public class Discipline {

	private String code;
	private String name;
	private String teacher;
	private String period;
	private String frequency;

	private String url;

	private List<Proof> proofs = new ArrayList<>();

	public Discipline() {
	}

	public Discipline(String code, String name, String teacher, String period, String frequency, String url) {
		this.code = code;
		this.name = name;
		this.teacher = teacher;
		this.period = period;
		this.frequency = frequency;
		this.url = url;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Proof> getProofs() {
		return proofs;
	}

	public void setProofs(List<Proof> proofs) {
		this.proofs = proofs;
	}

	public void addProof(Proof proof) {
		this.proofs.add(proof);
	}

	@Override
	public String toString() {
		return "Discipline [code=" + code + ", name=" + name + ", teacher=" + teacher + ", period=" + period
				+ ", frequency=" + frequency + ", url=" + url + ", proofs=" + proofs + "]";
	}

}
