package br.ufc.felipe.backendlogin;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.CookieHandler;
import java.net.CookieManager;

import br.ufc.felipe.backendlogin.model.Discipline;
import br.ufc.felipe.backendlogin.model.Proof;
import br.ufc.felipe.backendlogin.model.Student;

public class SIPPALogger extends AbsLogger {

	public static final String URL_ROOT = "https://sistemas.quixada.ufc.br/";
	public static final String URL_SITE = "https://sistemas.quixada.ufc.br/sippa/";
	public static final String URL_LOGIN = "https://sistemas.quixada.ufc.br/ServletCentral";
	public static final String URL_LOGIN_PARAMS = "login=:registration&senha=:password&conta=aluno&captcha=:captcha&comando=CmdLogin";
	public static final String URL_LOGOUT = "https://sistemas.quixada.ufc.br/ServletCentral";
	public static final String URL_LOGOUT_PARAMS = "comando=CmdLogout";
	public static final String URL_CAPTCHA = "https://sistemas.quixada.ufc.br/sippa/captcha.jpg";
	public static final String URL_INDEX = "https://sistemas.quixada.ufc.br/sippa/aluno_conferir_freq.jsp";
	public static final String URL_SIPPA_LOGO = "https://sistemas.quixada.ufc.br/sippa/images/sippa_logo.png";

	public SIPPALogger() {
		CookieHandler.setDefault(new CookieManager());
	}

	public void prepareCookies() throws Exception {
		getPageContent(URL_SITE);
	}
	
	public void login(final String registration, final String password, final String captcha, final LoginListener listener) {

		log("Login into account: " + registration + "...");

		doInBackground(new Runnable() {
			
			@Override
			public void run() {
								
				try {
					
					Response response = sendPost(URL_LOGIN, URL_LOGIN_PARAMS.replace(":registration", registration)
							.replace(":password", password).replace(":captcha", captcha));
					
					if(isIndexPage(response.getText())) {
						log("Login done!");
						if(listener != null)
							listener.loginSuccess(getStudentFromPage(response.getText()));
					} else {
						log("Login error!");
						if(listener != null)
							listener.loginError();
					}
					
				} catch (Exception e) {

					e.printStackTrace();
					log("Login error!");
					if(listener != null)
						listener.loginError();

				}

			}
			
		});

	}
	
	public void logout(LogoutListener listener) {
		
		try {
			
			sendPost(URL_LOGOUT, URL_LOGOUT_PARAMS);
			if(listener != null)
				listener.logoutSuccess();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			if(listener != null)
				listener.logoutError();
			
		}
		
	}
	
	public boolean isIndexPage(String page) {
		
		Document doc = Jsoup.parse(page);
		return doc.getElementsByClass("tabela_ver_freq").size() > 0;
			
	}
	
	public Student getStudentFromPage(String page) {
		
		log("Making a student...");
		try {
			
			Student student = new Student();
			Document doc = Jsoup.parse(page);
			
			Element table = doc.getElementsByClass("tabela_ver_freq").first();
			Elements trs = table.getElementsByTag("tbody").first().getElementsByTag("tr");
			
			for(Element tr: trs) {
				Discipline discipline = new Discipline();
				Elements tds = tr.getElementsByTag("td");
				discipline.setCode(tds.get(0).text());
				discipline.setName(tds.get(1).text());
				discipline.setTeacher(tds.get(2).text());
				discipline.setPeriod(tds.get(3).text());
				discipline.setFrequency(tds.get(4).text());
				discipline.setUrl(tds.get(0).getElementsByTag("a").first().attr("href").replace("../", URL_ROOT));
				student.addDiscipline(discipline);
				
				log("Adding discipline: " + discipline.getName());
				log("Entering in discipline page: " + discipline.getUrl());
				getPageContent(discipline.getUrl());
				Response response = getPageContent("../ServletCentral?comando=CmdVisualizarAvaliacoesAluno".replace("../", URL_ROOT));
				Document disciplineDocument = Jsoup.parse(response.getText());
				Element tableProofs = disciplineDocument.getElementsByClass("tabela_ver_freq").first();
				
				Elements ths = tableProofs.getElementsByTag("thead").first().getElementsByTag("tr").first().getElementsByTag("th");
				Elements tds2 = tableProofs.getElementsByTag("tbody").first().getElementsByTag("tr").first().getElementsByTag("td");
				
				boolean first = true;
				int index = 0;
				for(Element th: ths) {
					
					if(first) {
						student.setName(tds2.get(0).text());
					} else {
						
						Proof proof = new Proof();
						proof.setName(th.text());
						proof.setValue(tds2.get(index).text());
						discipline.addProof(proof);
						
					}
					
					first = false;
					index++;
				}
				
			}
			
			return student;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
		
	public interface LoginListener {

		void loginSuccess(Student student);
		void loginError();

	}
	
	public interface LogoutListener {

		void logoutSuccess();
		void logoutError();

	}

}
